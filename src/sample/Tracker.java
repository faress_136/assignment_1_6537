package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Tracker extends Application {
FitnessCalculations fitnessCalculations = new FitnessCalculations();
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Fitness Tracker");
        primaryStage.setScene(new Scene(root, 300, 275));
        Label AddActivity = new Label("Add Activity   ");
        Label type = new Label("Type ");
        Label time = new Label("Time taken to practice (min) ");
        Label totalCalBurnt = new Label();
        Label swimCalBurnt = new Label();
        Label runCalBurnt = new Label();
        Label kickCalBurnt = new Label();
        Label strCalBurnt = new Label();
        Label incHeratSwim = new Label();
        Label incHeartRun = new Label();
        Label incHeartKick = new Label();
        Label incHeartStr = new Label();
        Label totalHeartRate = new Label();
        Label sorts = new Label();
        Label l1 = new Label();
        Label l2 = new Label();
        Label l3 = new Label();
        Label l4 = new Label();
        Label l5 = new Label();
        Label l6 = new Label();
        Label l7 = new Label();
        Label l8 = new Label();
        TextField timetextfield = new TextField();
        Button add = new Button("Add");
        Button sort = new Button("Sort");
        ComboBox comboBox = new ComboBox<>();
        comboBox.getItems().addAll(
                "Swimming",
                "Running",
                "Kick_Boxing",
                "Strength_Training"
        );


        GridPane grid = new GridPane();
        grid.setVgap(4);
        grid.setVgap(4);
        grid.add(AddActivity, 0, 0);
        grid.add(comboBox, 1, 0);
        grid.add(time, 0, 2);
        grid.add(timetextfield, 1, 2);
        grid.add(add, 1, 3);
        grid.add(totalCalBurnt,0,4);
        grid.add(totalHeartRate,0,5);
        grid.add(swimCalBurnt,0,6);
        grid.add(incHeratSwim,0,7);
        grid.add(runCalBurnt,0,8);
        grid.add(incHeartRun,0,9);
        grid.add(kickCalBurnt,0,10);
        grid.add(incHeartKick,0,11);
        grid.add(strCalBurnt,0,12);
        grid.add(incHeartStr,0,13);
        grid.add(sort,2,3);
        grid.add(sorts,0,15);
        grid.add(l1,0,16);
        grid.add(l2,0,17);
        grid.add(l3,0,18);
        grid.add(l4,0,19);
        grid.add(l5,0,20);
        grid.add(l6,0,21);
        grid.add(l7,0,22);
        grid.add(l8,0,23);


        add.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if (comboBox.getValue().equals("Swimming"))
                {
                    swimCalBurnt.setText("Swimming Calories Burnt : "  + fitnessCalculations.calCalculator("Swimming",Integer.parseInt(timetextfield.getText())));
                    swimCalBurnt.setFont(Font.font("Verdana" , 15));
//                    System.out.println(fitnessCalculations.calCalculator("Swimming",Integer.parseInt(timetextfield.getText())));
                    totalCalBurnt.setText("Total Calories Burnt : " + fitnessCalculations.getTotalCaloriesBurnt());
                    totalCalBurnt.setFont(Font.font("Verdana" , 20));
                    incHeratSwim.setText("Increase Heart Rate of Swimming : " + fitnessCalculations.heartrateCalculator("Swimming",Integer.parseInt(timetextfield.getText())) + " beats/min");
                    incHeratSwim.setFont(Font.font("Verdana" , 15));
                    totalHeartRate.setText("Total Heart Rate : " + fitnessCalculations.getTotalHeartRate());
                    totalHeartRate.setFont(Font.font("Verdana" , 20));

                }
                else if (comboBox.getValue().equals("Running"))
                {
                    runCalBurnt.setText("Running Calories Burnt : " + fitnessCalculations.calCalculator("Running",Integer.parseInt(timetextfield.getText())));
//                System.out.println(fitnessCalculations.calCalculator("Running",Integer.parseInt(timetextfield.getText())));
                    runCalBurnt.setFont(Font.font("Verdana" , 15));
                    totalCalBurnt.setText("Total Calories Burnt : " + fitnessCalculations.getTotalCaloriesBurnt());
                    totalCalBurnt.setFont(Font.font("Verdana" , 20));
                    incHeartRun.setText("Increase Heart Rate of Running : " + fitnessCalculations.heartrateCalculator("Running",Integer.parseInt(timetextfield.getText())) + " beats/min");
                    incHeartRun.setFont(Font.font("Verdana" , 15));
                    totalHeartRate.setText("Total Heart Rate : " + fitnessCalculations.getTotalHeartRate());
                    totalHeartRate.setFont(Font.font("Verdana" , 20));

                }
                else if (comboBox.getValue().equals("Kick_Boxing"))
                {
                    kickCalBurnt.setText("Kick Boxing Calories Burnt : " + fitnessCalculations.calCalculator("Kick_Boxing",Integer.parseInt(timetextfield.getText())));
                    kickCalBurnt.setFont(Font.font("Verdana" , 15));
//                    System.out.println(fitnessCalculations.calCalculator("Kick_Boxing",Integer.parseInt(timetextfield.getText())));
                    totalCalBurnt.setText("Total Calories Burnt : " + fitnessCalculations.getTotalCaloriesBurnt());
                    totalCalBurnt.setFont(Font.font("Verdana" , 20));
                    incHeartKick.setText("Increase Heart Rate of KickBoxing : " + fitnessCalculations.heartrateCalculator("Kick_Boxing",Integer.parseInt(timetextfield.getText())) + " beats/min");
                    incHeartKick.setFont(Font.font("Verdana" , 15));
                    totalHeartRate.setText("Total Heart Rate : " + fitnessCalculations.getTotalHeartRate());
                    totalHeartRate.setFont(Font.font("Verdana" , 20));

                }
                else if (comboBox.getValue().equals("Strength_Training"))
                {
                    strCalBurnt.setText("Strength Training Calories Burnt : " + fitnessCalculations.calCalculator("Strength_Training", Integer.parseInt(timetextfield.getText())));
//                    System.out.println(fitnessCalculations.calCalculator("Strength_Training",Integer.parseInt(timetextfield.getText())));
                    strCalBurnt.setFont(Font.font("Verdana" , 15));
                    totalCalBurnt.setText("Total Calories Burnt : " + fitnessCalculations.getTotalCaloriesBurnt());
                    totalCalBurnt.setFont(Font.font("Verdana" , 20));
                    incHeartStr.setText("Increase Heart Rate of StrengthTraining : " + fitnessCalculations.heartrateCalculator("Strength_Training",Integer.parseInt(timetextfield.getText())) + " beats/min");
                    incHeartStr.setFont(Font.font("Verdana" , 15));
                    totalHeartRate.setText("Total Heart Rate : " + fitnessCalculations.getTotalHeartRate());
                    totalHeartRate.setFont(Font.font("Verdana" , 20));

                }
            }
        });
        sort.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                sorts.setText("Sorted Descending");
                sorts.setFont(Font.font("Verdana" , 20));
                Fares [] array = fitnessCalculations.sort();
                l1.setText(array[0].getName() + " Calories Burnt : " + array[0].getCaloriess());
                l1.setFont(Font.font("Verdana" , 15));
                l2.setText(array[0].getName() + " Heart Rate Increase : " + array[0].getHeartratee());
                l2.setFont(Font.font("Verdana" , 15));
                l3.setText(array[1].getName() + " Calories Burnt : " + array[1].getCaloriess());
                l3.setFont(Font.font("Verdana" , 15));
                l4.setText(array[1].getName() + " Heart Rate Increase : " + array[1].getHeartratee());
                l4.setFont(Font.font("Verdana" , 15));
                l5.setText(array[2].getName() + " Calories Burnt : " + array[2].getCaloriess());
                l5.setFont(Font.font("Verdana" , 15));
                l6.setText(array[2].getName() + " Heart Rate Increase : " + array[2].getHeartratee());
                l6.setFont(Font.font("Verdana" , 15));
                l7.setText(array[3].getName() + " Calories Burnt : " + array[3].getCaloriess());
                l7.setFont(Font.font("Verdana" , 15));
                l8.setText(array[3].getName() + " Heart Rate Increase : " + array[3].getHeartratee());
                l8.setFont(Font.font("Verdana" , 15));

            }
        });

Scene scene = new Scene(grid,800,800);
primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
