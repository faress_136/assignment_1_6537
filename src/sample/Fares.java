package sample;

public class Fares {
    String name;
    int caloriess;
    double heartratee;

    public Fares(String name) {
        this.name = name;
        this.caloriess = 0;
        this.heartratee = 0;
    }

//    public Fares(int caloriess) {
//        this.caloriess = 0;
//    }
//
//    public Fares(double heartratee) {
//        this.heartratee = 0;
//    }
//
//    public Fares(String name) {
//        this.name = name;
//    }


    public int getCaloriess() {
        return caloriess;
    }

    public void setCaloriess(int caloriess) {
        this.caloriess = caloriess;
    }

    public double getHeartratee() {
        return heartratee;
    }

    public void setHeartratee(double heartratee) {
        this.heartratee = heartratee;
    }

    public String getName() {
        return name;
    }
}
